<?php

/**
 * @file
 *
 * Module File for Slack RTM.
 */

use Drupal\Core\Render\Element;
use Drupal\slack_rtm\SlackRtmApi;


/**
 * Implements hook_menu_local_tasks_alter().
 */
function slack_rtm_menu_local_tasks_alter(&$data, $route_name) {
  // The route on the Slack RTM structure page.
  $routes = [
    'entity.slack_rtm_message.collection',
    'slack_rtm.slack_rtm_update_form',
    'entity.entity_view_display.slack_rtm_message.default',
    'slack_rtm_message.settings',
  ];

  // Remove the manage field and form display tabs.
  if (in_array($route_name, $routes)) {
    unset($data["tabs"][0]['field_ui.fields:overview_slack_rtm_message']);
    unset($data["tabs"][0]['field_ui.fields:form_display_overview_slack_rtm_message']);
  }
}

/**
 * Implements hook_cron().
 */
function slack_rtm_cron() {
  $items = (new SlackRtmApi())->getMessages(FALSE);
  $queue = \Drupal::queue('slack_rtm_queue');
  foreach($items as $item) {
    // Add to queue.
    $queue->createItem($item);
  }
}

/**
 * Prepares variables for Slack RTM Message templates.
 *
 * Default template: slack_rtm_message.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_slack_rtm_message(array &$variables) {
  // Fetch SlackRtmMessage Entity Object.
  $slack_rtm_message = $variables['elements']['#slack_rtm_message'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

