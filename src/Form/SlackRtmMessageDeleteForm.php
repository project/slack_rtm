<?php

namespace Drupal\slack_rtm\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Slack RTM Messages.
 *
 * @ingroup slack_rtm
 */
class SlackRtmMessageDeleteForm extends ContentEntityDeleteForm {


}
