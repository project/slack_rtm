<?php

namespace Drupal\slack_rtm;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for slack_rtm_message.
 */
class SlackRtmMessageTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
